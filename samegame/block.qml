import QtQuick 2.2 

Rectangle
{
	id: block
	property int type
	property bool dying: false

	color: type == 0 ? "red" : type == 1 ? "green" : type == 2 ? "blue" : "yellow" 
	visible: !dying

	Behavior on y 
	{
		NumberAnimation {
			duration: 100;
		}
	}

	Behavior on x
	{
		NumberAnimation {
			duration: 100;
		}
	}
}
