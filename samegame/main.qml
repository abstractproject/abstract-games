/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11
import QtQuick.Controls 2.4
import "game.js" as Game

Rectangle {
	id: window
	width: 360
	height: 360
	color: "gray"
	property int score: 0
	property bool winning: false
	property bool winning2: false
	property real n: msg.easy ? 10.0 : msg.medium ? 15 :  20.0
	property bool pause: false
	property int numcolors: msg.easy ? 3 : msg.medium ? 3 : 4

	SwipeView
	{
		id: swipeview
		currentIndex: 0
		anchors.fill: parent

		SinglePlayerStartupScreen
		{
			id: msg
			pause: window.pause
			objectName: "msg"

			onNewGame:
			{
				if (window.pause)
				{
					swipeview.currentIndex = 1;
					return;
				}

				swipeview.currentIndex = 1;
				Game.Restart();
			}
			
            onRestartGame: {
                swipeview.currentIndex = 1;
                Game.Restart();
            }
		}

		GameCanvas
		{
			id: canvas

			onClicked: {
				Game.Click(mouse.x,mouse.y);
			}
		}
	}

	QtObject
	{
		property int index: swipeview.currentIndex

		Behavior on index
		{
			ScriptAction
			{
				script:
				{
					if (swipeview.currentIndex == 1)
					{
						if (!window.pause)
						{
							Game.Restart();
						}
					}
					else
					{
						window.pause = true;
					}
				}
			}
		}
	}
}
