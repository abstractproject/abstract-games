/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*
	Small uillity functions.
*/

function Pause()
{
	window.pause = true;
	msg.visible = true;
	msg.text = "Game Paused";
}

function ToIndex(x, y)
{
	return [Math.floor(x / (window.width / window.n)), Math.floor(y / (window.height / window.n))];
}

function GetPos(i) {
        var r, c;
        var k = 1;

        for (var j = 0; j < window.n*window.n; j++)
        {
                if (j % window.n == 0 && j != 0) k++;
                if (i < k * window.n)
                {
                        c = k - 1;
                        r = i - ((k - 1) * window.n);
                        break;
                }
        }

        return [r, c];
}

function GetInvPos(r, c) {
        if (r >= window.n || r < 0 || c >= window.n || c < 0) return -1;

        return r + c * window.n
}

/*
	Core game functions.
*/

var board;
var component;

function Restart() {
	window.score = 0;

	if (board)
	{
		console.log("Hey-.");
		for (var i = 0; i < board.length; i++)
		{
			if (board[i]) board[i].destroy();
		}
	}

	board = new Array(window.n*window.n);
	component = Qt.createComponent("block.qml");

	for (var i = 0; i < window.n*window.n; i++) {
		board[i] = component.createObject(canvas, {"x": GetPos(i)[0] * (window.width / window.n), "y": GetPos(i)[1] * (window.height / window.n), 
		"width": window.width / window.n, "height": window.height / window.n, "type": Math.floor(Math.random() * window.numcolors)});
		
	}
}

var fillFound;
var floodBoard;

function Click(x, y) {
	fill(ToIndex(x, y)[0], ToIndex(x, y)[1], -1);
	if (fillFound != 0)
	{
		window.score += (fillFound - 1) * (fillFound - 1);
		shuffle();

		if (!check(0, window.n - 1, -1))
		{
			var none = true;
			for (var i = 0; i < window.n*window.n; i++)
			{
				if (board[i])
				{
					none = false;
					break;
				}
			}

			if (none)
			{
				if (msg.easy)
				{
					window.score += 500;
				}
				else if (msg.medium)
				{
					window.score += 1000;
				}
				else
				{
					window.score += 5000;
				}
			}

			msg.displayText("You Got " + window.score + " Points");

			swipeview.currentIndex = 0;
			window.pause = false;
		}
	}
}

function shuffle()
{
	for (var x = 0; x < window.n; x++) {
		var fallDist = 0;
		for (var y = window.n - 1; y >= 0; y--)
		{
			if (board[GetInvPos(x, y)] == null)
			{
				console.log("O>");
				fallDist++;
			}
			else
			{
				if (fallDist > 0)
				{
					var obj = board[GetInvPos(x, y)];
					obj.y = (y + fallDist) * (window.height / window.n);
					board[GetInvPos(x, y + fallDist)] = obj;
					board[GetInvPos(x, y)] = null;
					console.log("Müller.");
				}
			}
		}
	}

	var fallDist = 0;

	for (var x = 0; x < window.n; x++)
	{
		if (board[GetInvPos(x, window.n - 1)] == null) {
			fallDist++;
		}
		else {
			if (fallDist > 0) {
				for (var y = 0; y < window.n; y++)
				{
					var obj = board[GetInvPos(x, y)];
					if (obj == null)
						continue;
					obj.x = (x - fallDist) * (window.width / window.n);
					board[GetInvPos(x - fallDist,y)] = obj;
					board[GetInvPos(x, y)] = null;
				}
			}
		}
	}
}

function fill(x, y, tp) {
	var index = GetInvPos(x, y);

	if (board[index] == null)
		return;
	if (x >= board.n || x < 0 || y >= board.n || y < 0) return false;

	console.log("After null-check.");

	var first = false;
	if (tp == -1) {
		first = true;
		tp = board[index].type;
		fillFound = 0;
		floodBoard = new Array(window.n*window.n);
		console.log("In first time");
	}

	console.log(index);

	if (floodBoard[index] == 1 || (!first && tp != board[index].type))
		return;

	console.log("After flood-check");

	floodBoard[index] = 1;
	fill(x + 1, y, tp);
	fill(x - 1, y, tp);
	fill(x, y + 1, tp);
	fill(x, y - 1, tp);

	if (first == true && fillFound == 0)
		return;

	console.log("After fill-check");

	board[index].dying = true
	board[index] = null;
	fillFound += 1;

	console.log("End.");

	/*if (board.children[i] == undefined /*|| board.children[i].visible == false || board.children[i].dyin) return;

	if (board.children[i].visible == false)
	{
		var j = i;
		var h = false;

		while (!board.children[i].visible)
		{
			i = GetInvPos(GetPos(i)[0] + 1, GetPos(i)[1]);
			if (board.children[i] == undefined) {
				console.log(i);
				h = true;
				break;
			}
		}

		if (!h) fill(i, tp, z);

		h = false
		i = j;

		while (!board.children[i].visible)
		{
			i = GetInvPos(GetPos(i)[0] - 1, GetPos(i)[1]);
			if (board.children[i] == undefined) {
				console.log(i);
				h = true;
				break;
			}
		}

		if (!h) fill(i, tp, z);

		h = false;
		i = j;

		while (!board.children[i].visible)
		{
			i = GetInvPos(GetPos(i)[0], GetPos(i)[1] + 1);
			if (board.children[i] == undefined) {
				console.log(i);
				h = true;
				break;
			}
		}

		if (!h) fill(i, tp, z);

		h = false;
		i = j;

		while (!board.children[i].visible)
		{
			i = GetInvPos(GetPos(i)[0], GetPos(i)[1] - 1);

			if (board.children[i] == undefined) {
				console.log(i);
				h = true;
				break;
			}

		}

		if (!h) fill(i, tp, z);

		return;
	}

	var first = false;
	if (tp == -1) {
		first = true;
		tp = board.children[i].type;

		fillFound = 0;
		floodBoard = new Array(window.n*window.n);
	}
	if (floodBoard[i] == 1 || (!first && tp != board.children[i].type))
		return;
	floodBoard[i] = 1;
	fill(GetInvPos(GetPos(i)[0], GetPos(i)[1] + 1), tp, 0);
	fill(GetInvPos(GetPos(i)[0], GetPos(i)[1] - 1), tp, 0);
	fill(GetInvPos(GetPos(i)[0] + 1, GetPos(i)[1]), tp, 0);
	fill(GetInvPos(GetPos(i)[0] - 1, GetPos(i)[1]), tp, 0);
	fill(GetInvPos(GetPos(i)[0] + 1, GetPos(i)[1] + 1), tp, 0);
	fill(GetInvPos(GetPos(i)[0] - 1, GetPos(i)[1] + 1), tp, 0);
	fill(GetInvPos(GetPos(i)[0] + 1, GetPos(i)[1] - 1), tp, 0);
	fill(GetInvPos(GetPos(i)[0] - 1, GetPos(i)[1] - 1), tp, 0);

	/*if (i == 0)
	{
		console.log("Debug:Zero Index Floodfill.");
		return;
	}

	if (first == true && fillFound == 0)
		return;

	board.children[i].dying = true;

	fillFound += 1;*/

	
}

function check(x, y, tp)
{
	var index = GetInvPos(x, y);
	if (board[index] == null)
	{
		return false;
	}
	if (x >= board.n || x < 0 || y >= board.n || y < 0) return false;

	var type = board[index].type;
	if (tp == type)
		return true;

	return check(x + 1, y, type) || check(x, y - 1, board[GetInvPos(x, y)].type);

}
