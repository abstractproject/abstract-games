/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11 
import QtQuick.Controls 2.4
import "game.js" as Game

Rectangle {
	id: window
	width: 360
	height: 360
	color: "gray"

	QtObject
	{
		id: obj1
		property string othercolor
		property string syscolor
		property string _color
		property string c1
		property string c2
	}

	SequentialAnimation
	{
		running: board.moving2black

		ScriptAction
		{
			script:
			{
				msg.visible = false;
			}
		}

		RotationAnimation
		{
			target: board
			properties: "rotation"
			duration: 750
			from: 0
			to: 180 
		}

		ScriptAction
		{
			script:
			{
				msg.visible = true;
				board.moving2black = false;
			}
		}
	}

	SequentialAnimation
	{
		running: board.moving2white

		ScriptAction
		{
			script:
			{
				msg.visible = false;
			}
		}

		RotationAnimation
		{
			target: board
			properties: "rotation"
			duration: 750
			from: 180
			to: 0
		}

		ScriptAction
		{
			script:
			{
				msg.visible = true;
				board.moving2white = false;
			}
		}
	}

	SwipeView
	{
		id: swipeview
		currentIndex: 0
		anchors.fill: parent

		MutiplePlayerStartupScreen
		{
			id: msg
			objectName: "msg" 
			pause: board.pause
			property bool playaiwhite: msg.spectrumonone
			property bool playaiblack: msg.spectrumontwo
        
			onNewGame: {
				if (board.pause)
				{
					swipeview.currentIndex = 1;
					return;
				}

				swipeview.currentIndex = 1;
				Game.Restart();
			}
			
			onRestartGame: {
                swipeview.currentIndex = 1;
                Game.Restart();
            }            
		}

		Grid {
			id: board
			rows: 8
			columns: 8

			property string turn: "#ffffff"
			property bool check: false
			property bool moving2black: false
			property bool moving2white: false
			property bool first: false
			property var pawns_white: [true, true, true, true, true, true, true, true]
			property var pawns_black: [true, true, true, true, true, true, true, true]
			property bool pause: false

			Repeater {
				model: 64

				Rectangle {
					width: window.width/8
					height: window.height/8
					id: rect

					property bool white: false 
					property bool selected: false
					property bool nx: false
					property bool fx: false

					color: white ? "lightgray" : "darkgray"

					Text {
						id: text
						anchors.centerIn: parent
						opacity: 0
					}

					Image {
						id: image 
						anchors.centerIn: parent
						source: Game.image(text.text, text.color)
						smooth: true
						sourceSize.width: 45 
						sourceSize.height: 45
						rotation: board.rotation
					}


					SequentialAnimation
					{
						running: rect.fx

						NumberAnimation {
							target: image 
							properties: "opacity"
							duration: 2500
							from: 0 
							to: 1
						}

						ScriptAction
						{
							script:
							{
								rect.fx = false;
							}
						}
					}

					border {
						width: 2 
						color: "#00000000"

						Behavior on color {
							ColorAnimation {
								duration: 200
							}
						}
					}

					MouseArea {
						anchors.fill: parent;
						onClicked: {
							if (board.winning)
							{
								board.winning1 = true;
							}
							if (board.children[index].children[0].text != "") {
								if (board.children[index].selected) {
									board.children[index].border.color = "#00000000";
									board.children[index].selected = false;
								} else {
									for (var i = 0; i < 64; i++)
										if (board.children[i].selected) {
											Game.MakeMove(i, index);
											return;
										}
									board.children[index].border.color = "black";
									board.children[index].border.width = "2";
									board.children[index].selected = true;
								}
							} else {
								for (var i = 0; i < 64; i++) 
									if (board.children[i].selected)
										Game.MakeMove(i, index);
							}
						}
					}
				}
			}
		}

	}

	QtObject
	{
		property int index: swipeview.currentIndex

		Behavior on index
		{
			ScriptAction
			{
				script:
				{
					if (swipeview.currentIndex == 1)
					{
						if (!board.pause)
						{
							Game.Restart();
						}
					}
					else
					{
						board.pause = true;
					}
				}
			}
		}
	}
}
