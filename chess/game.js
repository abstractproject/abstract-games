/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*

	Small utility functions.
*/

function max(x, y)
{
	return x > y ? x : y;
}

function min(x, y)
{
	return x < y ? x : y;
}

function sidevalue(color)
{
	var value = 0;
	var p;

	for (var i = 0; i < 8; i++)
	{
		for (var j = 0; j < 8; j++)
		{
			p = board.children[GetInvPos(i, j)].children[0].text;
			if (j == 0 || j == 1 || j == 6 || j == 7)
				value--;

			if (p == "pawn")
			{
				if (x > 1 && X < 6) value += 11
				else value += 10
			}
			else if (p == "knight")
			{
				if (x == 0 || x == 7) value += 30
				else value += 31
			}
			else if (p == "bishop")
			{
				value += 30 
			}
			else if (p == "rook")
			{
				if (x == 3 || x == 4) value += 52
				else if (x == 0 || x == 7) value += 51
				else value += 50
			}
			else if (p == "queen")
			{
				value += 120;
			}
			else if (p == "king")
			{
				if (color == "#ffffff")
				{
					if (y == 0) value += 505
					else value += 500
				}
				else
				{
					if (y == 7) value += 505
					else value += 500
				}
			}
		}
	}

	return value;
}

function heuristic()
{
	return sidevalue(board.turn) - sidevalue(board.turn == "#ffffff" ? "#000000" : "#ffffff")
}

function image(text, color) {
	if (color == "#ffffff")
	{
		if (text == "pawn")
		{
			return "images/pawn_white.svg";
		}
		else if (text == "rook")
		{
			return "images/rook_white.svg";
		}
		else if (text == "knight")
		{
			return "images/knight_white.svg";
		}
		else if (text == "bishop")
		{
			return "images/bishop_white.svg";
		}
		else if (text == "king")
		{
			return "images/king_white.svg";
		}
		else if (text == "queen")
		{
			return "images/queen_white.svg";
		}
	}
	if (color == "#000000")
	{
		if (text == "pawn")
		{
			return "images/pawn_black.svg";
		}
		else if (text == "rook")
		{
			return "images/rook_black.svg";
		}
		else if (text == "knight")
		{
			return "images/knight_black.svg";
		}
		else if (text == "bishop")
		{
			return "images/bishop_black.svg";
		}
		else if (text == "king")
		{
			return "images/king_black.svg";
		}
		else if (text == "queen")
		{
			return "images/queen_black.svg";
		}
	}

	return "";
}

function GetPos(i) {
	/*
		Get the (x, y) pos from the index i.
	*/

	var row = 0;
	var column = 0;

	if (i < 8) {
		column = 0;
		row = i;
	} else if (i < 16) {
		column = 1;
		row = i - 8;
	} else if (i < 24) {
		column = 2;
		row = i - 16;
	} else if (i < 32) {
		column = 3;
		row = i - 24;
	} else if (i < 40) {
		column = 4;
		row = i - 32;
	} else if (i < 48) {
		column = 5;
		row = i - 40;
	} else if (i < 56) {
		column = 6;
		row = i - 48;
	} else if (i < 64) {
		column = 7;
		row = i - 56;
	}

	return [row, column];
}

function GetInvPos(r, c) {
	var i = 0;

	if (r >= 8 || c >= 8 || r == -1 || c == -1 || r < 0 || c < 0) return -1;

	if (c == 0) i = r;
	else if (c == 1) i = r + 8;
	else if (c == 2) i = r + 8*2;
	else if (c == 3) i = r + 8*3;
	else if (c == 4) i = r + 8*4;
	else if (c == 5) i = r + 8*5;
	else if (c == 6) i = r + 8*6;
	else if (c == 7) i = r + 8*7;
	else if (c == 8) i = r + 8*8;
	else return -1;

	return i;
}

function CheckPath(pos)
{
	if (pos[0][0] == pos[1][0])
	{
		if (pos[0][1] < pos[1][1])
		{
			for (var y = pos[0][1] + 1; y < pos[1][1]; y++)
			{
				if (board.children[GetInvPos(pos[0][0], y)].children[0].text != "")
				{
					return false;
				}
			}
		}

		else if (pos[0][1] > pos[1][1])
		{
			for (var y = pos[1][1] + 1; y < pos[0][1]; y++)
			{
				if (board.children[GetInvPos(pos[0][0], y)].children[0].text != "")
				{
					return false;
				}
			}
		}
	}
	else if (pos[0][1] == pos[1][1])
	{
		if (pos[0][0] < pos[1][0])
		{
			for (var x = pos[0][0] + 1; x < pos[1][0]; x++)
			{
				if (board.children[GetInvPos(x, pos[0][1])].children[0].text != "")
				{
					return false;
				}
			}
		}
		else if (pos[0][0] > pos[1][0])
		{
			for (var x = pos[1][0] + 1; x < pos[0][0]; x++)
			{
				if (board.children[GetInvPos(x, pos[0][1])].children[0].text != "")
				{
					return false;
				}
			}
		}
	}
	else if (Math.abs(pos[0][0] - pos[1][0]) == Math.abs(pos[0][1] - pos[1][1]))
	{
		function max(a, b)
		{
			return a > b ? a : b;
		}

		if (pos[0][0] < pos[1][0] && pos[0][1] < pos[1][1])
		{
			/*
			for (var x = pos[0][0] + 1, y = pos[0][1] + 1; 
				max(pos[1][0], pos[1][1]) == pos[1][0] ? x < pos[1][0] : y < pos[1][1]; x++, y++)
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "")
				{
					return false;
				}
			}
			*/

			var x = pos[0][0] + 1;
			var y = pos[0][1] + 1;

			while (x < pos[1][0] && y < pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x++;
				y++;
			}
		}
		else if (pos[0][0] > pos[1][0] && pos[0][1] < pos[1][1])
		{
			/*
			for (var x = pos[1][0] + 1, y = pos[0][1] + 1; 
				max(pos[0][0], pos[1][1]) == pos[0][0] ? x < pos[0][0] : y < pos[1][1]; x++, y++)
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "")
				{
					return false;
				}
			}
			*/

			var x = pos[0][0] - 1;
			var y = pos[0][1] + 1;

			while (x >= pos[1][0] && y < pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x--;
				y++;
			}
		}
		else if (pos[0][0] < pos[1][0] && pos[0][1] > pos[1][1])
		{
			/*
			var x = pos[0][0] + 1;
			var y = pos[1][1] + 1;

			while ((max(pos[1][0], pos[0][1]) == pos[1][0]) ? (x < pos[1][0]) : (y < pos[0][1]))
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x++;
				y++;
			}

			return true;
			*/

			var x = pos[0][0] + 1;
			var y = pos[0][1] - 1;

			while (x < pos[1][0] && y >= pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x++;
				y--;
			}
		}
		else if (pos[0][0] > pos[1][0] && pos[0][1] > pos[1][1])
		{
			/*
			var x = pos[1][0] + 1;
			var y = pos[1][1] + 1;

			while ((max(pos[0][0], pos[0][1]) == pos[0][0]) ? (x < pos[0][0]) : (y < pos[0][1]))
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x++;
				y++;
			}
			*/

			var x = pos[0][0] - 1;
			var y = pos[0][1] - 1;

			while (x >= pos[1][0] && y >= pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x--;
				y--;
			}
		}
	}

	return true;
}

var a;
var b;
var movevalue;
var r;


function PlayAI(v, s, f)
{
//function PlayAI() {
	/*
		Method for making new moves.
	*/

	if (f == true)
	{
		a = 1000;
		b = -1000;
		r = 2;
	}
	
	if (v == true)
	{
		movevalue = 1000;
	}
	else
	{
		movevalue = -1000;
	}

	var moves = new Array();
	obj1.othercolor = board.turn;
	var color = obj1.othercolor == "#ffffff" ? "#000000" : "#ffffff";

	for (var i = 0; i < 64; i++) {
		for (var j = 0; j < 64; j++) {

			if (
				CheckMove(
					[GetPos(i), GetPos(j)],
					board.children[i].children[0].text,
					board.children[i].children[0].color,
					board.children[j].children[0].text,
					board.children[j].children[0].color
				)
			)
			{
				moves.push([i, j]);
			}
			
		}
	}

	var TakeMoves = new Array();
	var OtherMoves = new Array();
	var CheckMoves = new Array();
	var CheckMateMoves = new Array();

	var bestmove = [-1, -1];

	var move;
	var piece;
	var otherpiece;
	var first
	var next;
	
	for (var i = 0; i < moves.length; i++)
	{
		move = moves[i];//moves[Math.floor(Math.random() * moves.length)];
		piece = board.children[move[0]].children[0].text;
		otherpiece = board.children[move[1]].children[0].text;
		obj1.syscolor = board.children[move[0]].children[0].color;
		obj1._color = board.children[move[1]].children[0].color;

		board.children[move[0]].children[0].text = "";
		board.children[move[1]].children[0].text = piece;
		board.children[move[1]].children[0].color = obj1.syscolor;

		if (msg.playaiblack)
		{
			board.turn = "#000000";
		}
		else
		{
			board.turn = "#ffffff";
		}

		next = board.turn == "#ffffff" ? "#000000" : "#ffffff";
		first = board.turn;

		board.turn = next;

		if (!Check(first))
		{
			board.turn = first;

			if (otherpiece != "")
			{
				TakeMoves.push(move);
			} else if (Check(next))
			{
				if (CheckMate(next))
				{
					CheckMateMoves.push(move);
				} else
				{
					CheckMoves.push(move);
				}
			}
			else if (otherpiece != "")
			{
				TakeMoves.push(move);
			}
			else
			{
				OtherMoves.push(move);
			}

			/*if (v)
			{
				var oldmovevalue = movevalue;
				r--;
				if (r == 0)
				{
					movevalue = heuristic();
				}
				else
				{
					PlayAI(false, false, false);
				}

				if (movevalue <= oldmovevalue) bestmove = move;

				a = max(a, movevalue);

				if (a > b)
				{
					movevalue = a;
					bestmove = move;
					board.children[move[0]].children[0].text = piece; board.children[move[1]].children[0].color = obj1._color; board.children[move[1]].children[0].text = otherpiece;
					break;
				}
			}
			else
			{
				var oldmovevalue = movevalue;
				r--;
				if (r == 0)
				{
					movevalue = heuristic();
				}
				else
				{
					PlayAI(true, false, false);
				}
				if (oldmovevalue <= movevalue) bestmove = move;
				b = min(b, movevalue);

				if (b < a)
				{
					movevalue = b;
					bestmove = move;
					board.children[move[0]].children[0].text = piece; board.children[move[1]].children[0].color = obj1._color; board.children[move[1]].children[0].text = otherpiece;
					break;
				}
			}*/
		}

		board.children[move[0]].children[0].text = piece;
		board.children[move[1]].children[0].color = obj1._color;
		board.children[move[1]].children[0].text = otherpiece;

	}

	if (s == false) return;

	var fmove;// = bestmove;

	
	if (CheckMateMoves.length != 0)
	{
		fmove = CheckMateMoves[Math.floor(Math.random() * CheckMateMoves.length)];
	} else if (CheckMoves.length != 0)
	{
		fmove = CheckMoves[Math.floor(Math.random() * CheckMoves.length)];
	} else if (TakeMoves.length != 0)
	{
		fmove = TakeMoves[Math.floor(Math.random() * TakeMoves.length)];
	} else
	{
		fmove = OtherMoves[Math.floor(Math.random() * OtherMoves.length)];
	}
	

//	fmove = OtherMoves[Math.floor(Math.random() * OtherMoves.length)];
	//

	var piece_2 = board.children[fmove[0]].children[0].text;


	board.children[fmove[0]].children[0].text = "";
	board.children[fmove[1]].children[0].text = piece_2;
	board.children[fmove[1]].children[0].color = obj1.othercolor;

	board.children[fmove[0]].nx = true;
	board.children[fmove[1]].fx = true;


	console.log(fmove);
	console.log([GetPos(fmove[0]), GetPos(fmove[1])]);
	return [GetPos(fmove[0]), GetPos(fmove[1])];
}

function CheckMate(color) {
	/*
		Checks for CheckMate.
	*/

	var othercolor = color == "#ffffff" ? "#000000" : "#ffffff";
	var checkmate = true;

	for (var j = 0; j < 64; j++) {
		if (board.children[j].children[0].text != "" && board.children[j].children[0].color == color) {
			for (var i = 0; i < 64; i++) {
				board.turn = color
				if (
					CheckMove(
						[GetPos(j), GetPos(i)], 
						board.children[j].children[0].text, 
						color, 
						board.children[i].children[0].text, 
						board.children[i].children[0].color
					)
				)
				{
						if (board.children[i] == undefined || board.children[j] == undefined)
						{
							console.log("ERROR!ERROR!ERROR!SOS!SOS!");
							continue;
						}

						var p1 = board.children[j].children[0].text;
						obj1.c1 = board.children[j].children[0].color;

						var p2 = board.children[i].children[0].text;
						obj1.c2 = board.children[i].children[0].color;

						board.children[i].children[0].text = p1;
						board.children[i].children[0].color = obj1.c1;

						board.children[j].children[0].text = "";

						board.turn = othercolor;

						if (!Check(color))
							checkmate = false;

						board.turn = color;

						board.children[i].children[0].text = p2;
						board.children[i].children[0].color = obj1.c2;

						board.children[j].children[0].text = p1;
						board.children[j].children[0].color = obj1.c1;
						//if (!checkmate) return checkmate;
				}
			}
		}
	}

	return checkmate;
}

function Check(color) {
	/*
		Check's for Check.
	*/

	var othercolor = color == "#ffffff" ? "#000000" : "#ffffff";
	var j = 0;
	for (; j < 64; j++) {
		if (board.children[j].children[0].text == "king" && board.children[j].children[0].color == color)
			break;
	}

	for (var i = 0; i < 64; i++) {
		if (board.children[i].children[0].text != "" && board.children[i].children[0].color == othercolor) {
			if (CheckMove([GetPos(i), GetPos(j)], board.children[i].children[0].text, othercolor, "king", color))
				return true;
		}
	}

	return false;
}

function CheckMove(pos, piece, color, otherpiece, othercolor) {
	/*
		Checks to make sure move is a valid move.
	*/

	var accept = false;
	var taking = otherpiece != "";

	if (color == "#ffffff") {
			if (piece == "pawn") {
			if (!taking) {
				if (pos[0][1] + 1 == pos[1][1] && pos[0][0] == pos[1][0])
					accept = true;
				else if (board.pawns_white[pos[0][0]] == true)
				{
					if (pos[0][1] + 2 == pos[1][1] && pos[0][0] == pos[1][0])
						accept = true;
				}
			} else {
				if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == -1)
					accept = true;
				else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == -1)
					accept = true;
			}
		} else if (piece == "rook") {
			if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "king") {
			if (pos[0][1] + 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] - 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
		else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == -1)
			accept = true;
		else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == -1)
			accept = true;
			else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
	   } else if (piece == "bishop") {
			if (Math.abs(pos[0][0] - pos[1][0]) == Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
		} else if (piece == "queen") {
			if (Math.abs(pos[0][0] - pos[1][0]) == Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
			else if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "knight") {
			if (pos[0][0] + 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
		}
	} else {
		if (piece == "pawn") {
			if (!taking) {
				if (pos[0][1] - 1 == pos[1][1] && pos[0][0] == pos[1][0])
					accept = true;
				else if (board.pawns_black[pos[0][0]] == true)
				{
					if (pos[0][1] - 2 == pos[1][1] && pos[0][0] == pos[1][0])
						accept = true;
				}
			} else {
				if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == 1)
					accept = true;
				else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == 1)
					accept = true;
			}
		} else if (piece == "rook") {
			if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "king") {
			if (pos[0][1] + 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] - 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
			else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == -1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == -1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
		} else if (piece == "bishop") {
			if (Math.abs(pos[0][0] - pos[1][0]) ==  Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
		} else if (piece == "queen") {
			if (Math.abs(pos[0][0] - pos[1][0]) ==  Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
			else if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "knight") {
			if (pos[0][0] + 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
		}
	}

	if (taking)
		if (othercolor == color)
			accept = false;

	if (board.turn != color || !CheckPath(pos))
		accept = false;

	return accept;
}

/*
	Restart the game.
*/

function Restart() {
	board.turn = "#ffffff";
	board.first = true;

	board.pawns_white = [true, true, true, true, true, true, true, true];
	board.pawns_black = [true, true, true, true, true, true, true, true];

	var column = 0;
	var row = 0;

	for (var i = 0; i < 64; i++) {
		row = GetPos(i)[0];
		column = GetPos(i)[1];

		board.children[i].children[0].text = "";
		board.children[i].nx = false;
		board.children[i].fx = false;

		if (column % 2 == 1 && row % 2 == 0) {
			board.children[i].white = false;
		} else if (column % 2 == 0 && row % 2 == 1) {
			board.children[i].white = false;
		} else {
			board.children[i].white = true;
		}

		if (column == 0) {
			if (row == 0)
				board.children[i].children[0].text = "rook";
			if (row == 1)
				board.children[i].children[0].text = "knight";
			if (row == 2)
				board.children[i].children[0].text = "bishop";
			if (row == 3)
				board.children[i].children[0].text = "queen";
			if (row == 4)
				board.children[i].children[0].text = "king";
			if (row == 5)
				board.children[i].children[0].text = "bishop";
			if (row == 6)
				board.children[i].children[0].text = "knight";
			if (row == 7)
				board.children[i].children[0].text = "rook";

			board.children[i].children[0].color = "white";

		} else if (column == 1) {
			board.children[i].children[0].text = "pawn";
			board.children[i].children[0].color = "white";
		} else if (column == 6) {
			board.children[i].children[0].text = "pawn";
			board.children[i].children[0].color = "black";
		} else if (column == 7) {
			if (row == 0)
				board.children[i].children[0].text = "rook";
			if (row == 1)
				board.children[i].children[0].text = "knight";
			if (row == 2)
				board.children[i].children[0].text = "bishop";
			if (row == 3)
				board.children[i].children[0].text = "queen";
			if (row == 4)
				board.children[i].children[0].text = "king";
			if (row == 5)
				board.children[i].children[0].text = "bishop";
			if (row == 6)
				board.children[i].children[0].text = "knight";
			if (row == 7)
				board.children[i].children[0].text = "rook";

			board.children[i].children[0].color = "black";
		}

		board.children[i].border.width = "0";
		board.children[i].selected = false;
	}

	if (msg.playaiwhite) {
		PlayAI(true, true, true);
		//PlayAI();
		board.turn = "#000000";
		board.moving2black = true;
	}
}

/*
	Does the user's move.
*/

function MakeMove(firstIndex, lastIndex) {
	var piece = board.children[firstIndex].children[0].text;
	var color = board.children[firstIndex].children[0].color;
	var otherpiece = board.children[lastIndex].children[0].text;
	var othercolor = board.children[lastIndex].children[0].color;
	obj1.othercolor = board.children[lastIndex].children[0].color;
	var pos = [ GetPos(firstIndex), GetPos(lastIndex) ];

	if (CheckMove(pos, piece, color, otherpiece, othercolor)) {
		board.children[firstIndex].children[0].text = "";
		board.children[lastIndex].children[0].text = piece;
		board.children[lastIndex].children[0].color = color;

		board.children[firstIndex].selected = false;
		board.children[firstIndex].border.width = "0";

		if (piece == "pawn" && pos[1][1] == 7 && color == "#ffffff") {
			board.children[lastIndex].children[0].text = "queen";
		} else if (piece == "pawn" && pos[1][1] == 0 && color == "#000000") {
			board.children[lastIndex].children[0].text = "queen";
       		}

		board.check = false;

		var next = board.turn == "#ffffff" ? "#000000" : "#ffffff";
		var first = board.turn;

		board.turn = next;
		if (Check(first)) {
			board.children[firstIndex].children[0].text = piece;
			board.children[lastIndex].children[0].text = otherpiece;
			board.children[lastIndex].children[0].color = obj1.othercolor;
			board.children[firstIndex].selected = true; 
			board.children[firstIndex].border.width = "2";
			board.turn = first;
			return;
		} else
			board.turn = first;

		board.check = Check(next);
		if (board.check) {
			if (CheckMate(next)) {
				if (next == "#ffffff") {
					if (!msg.playaiblack)
						msg.displayText("Black Won");
					else
						msg.displayText("Spectrum Won");
				} else {
					if (!msg.playaiwhite)
						msg.displayText("White Won");
					else
						msg.displayText("Spectrum Won");

					swipeview.currentIndex = 0;
					board.pause = false;
				}
				return;
			}
			else
			{
				if (next == "#ffffff")
				{
					if (!msg.playaiwhite)
					{
						msg.displayText("White in Check");
					}
					else
					{
						msg.displayText("Spectrum in Check");
					}
				}
				else
				{
					if (!msg.playaiblack)
					{
						msg.displayText("Black in Check");
					}
					else
					{
						msg.displayText("Spectrum in Check");
					}
				}
			}
		}
		
		if (piece == "pawn" && color == "#ffffff")
		{
			board.pawns_white[pos[0][0]] = false;
		}
		else if (piece == "pawn" && color == "#000000")
		{
			board.pawns_black[pos[0][0]] = false;
		}
	}
	else
	{
		return;
	}

	board.children[firstIndex].nx = true; 
	board.children[lastIndex].fx = true;

	if (board.turn == "#ffffff" && !(msg.playaiblack || msg.playaiwhite))
		board.moving2black = true; // XXX 
	else if (!(msg.playaiblack || msg.playaiwhite))
		board.moving2white = true;

	board.turn = next;

	if (msg.playaiblack || msg.playaiwhite) {
		var indexes;
		indexes = PlayAI(true, true, true);
	        piece = board.children[GetInvPos(indexes[1][0], indexes[1][1])].children[0].text;

		if (piece == "pawn" && indexes[1][1] == 7 && msg.playaiwhite) {
			board.children[GetInvPos(indexes[1][0], indexes[1][1])].children[0].text = "queen";
		} else if (piece == "pawn" && indexes[1][1] == 0 && msg.playaiblack) {
			console.log(indexes[1][1]);
			board.children[GetInvPos(indexes[1][0], indexes[1][1])].children[0].text = "queen";
		}
		
		board.check = Check(first);
		if (board.check) {
			if (CheckMate(first)) {
				if (first == "#ffffff") {
					if (!msg.playaiblack)
						msg.displayText("Black Won");
					else
						msg.displayText("Spectrum Won");
				} else {
					if (!msg.playaiwhite)
						msg.displayText("White Won");
					else
						msg.displayText("Spectrum Won");
				}
				
				swipeview.currentIndex = 0;
				board.pause = false;
				return;
			}
			else
			{
				if (first == "#ffffff")
				{
					if (!msg.playaiwhite)
					{
						msg.displayText("White in Check");
					}
					else
					{
						msg.display("Spectrum in Check");
					}
				}
				else
				{
					if (!msg.playaiblack)
					{
						msg.displayText("Black in Check");
					}
					else
					{
						msg.display("Spectrum in Check");
					}
				}
			}
		}

		board.turn = first;
		console.log(indexes);

		if (piece == "pawn" && msg.playaiwhite)
   		{
   	    	 	board.pawns_white[indexes[0][0]] = false;
  	  	}
   		else if (piece == "pawn" && msg.playaiblack)
  	  	{
    			board.pawns_black[indexes[0][0]] = false;
   		}
	}
}
