/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11
import QtQuick.Controls 2.4
import "game.js" as Game

Rectangle {
	id: window
	width: 360
	height: 360
	color: "gray"

	SwipeView
	{
		id: swipeview
		currentIndex: 0
		anchors.fill: parent

		SinglePlayerStartupScreen
		{
			id: msg
			pause: board.pause
			objectName: "msg"

			onNewGame:
			{
				if (board.pause)
				{
					swipeview.currentIndex = 1;
					return;
				}

				swipeview.currentIndex = 1;
				Game.Restart();
			}
			
            onRestartGame: {
                swipeview.currentIndex = 1;
                Game.Restart();
            }
		}

		Grid {
			id: board
			rows: board.n
			columns: board.n

			property int mines: msg.easy ? 5 : msg.medium ? 10 : 30
			property int n: msg.easy ? 10 : msg.medium ? 12 : 15
			property bool pause: false

			Repeater {
				model: board.n*board.n

				Rectangle {
					width: window.width/board.n
					height: window.height/board.n
					color: "gray"
					id: rect

					border {
						color: "black"
						width: 1
					}

					Text {
						id: text
						property bool bomb: false
						property bool nb: false
						property bool fb: false
						property bool nfb: false
						property string oldtext: ""
						property int time
						anchors.centerIn: parent
						color: "white"
						visible: false
						font.pixelSize: 15
						font.weight: Font.Bold

						ParallelAnimation {
							running: text.bomb

							NumberAnimation {
								target: text 
								property: "visible"
								from: 0
								to: 1
								duration: text.time
							}

							ColorAnimation {
								target: rect 
								property: "color"
								from: "gray"
								to: "red"
								duration: text.time 
							}
						}

					
						ColorAnimation {
							running: text.nb
							target: rect 
							properties: "color"
							duration: 1500
							from: "gray"
							to: "green"
						}

						ColorAnimation {
							running: text.fb
							target: rect
							properties: "color"
							duration: 1500
							from: "gray"
							to: "blue"
						}

						ColorAnimation {
							running: text.nfb
							target: rect
							properties: "color"
							duration: 1500
							from: "blue"
							to: "gray"
						}
					}

					MouseArea {
						anchors.fill: parent
						acceptedButtons: Qt.LeftButton | Qt.RightButton

						onClicked: {
							if (mouse.button == undefined || mouse.button == Qt.RightButton)
							{
								console.log("flag");
								Game.Click(index, true);
						
							}
							else
							{
								console.log("Click");
								Game.Click(index, false);
							}
						}
					}
				}
			}
		}
	}

	QtObject
	{
		property int index: swipeview.currentIndex

		Behavior on index
		{
			ScriptAction
			{
				script:
				{
					if (swipeview.currentIndex == 1)
					{
						if (!board.pause)
						{
							Game.Restart();
						}
					}
					else
					{
						board.pause = true;
					}
				}
			}
		}
	}
}
