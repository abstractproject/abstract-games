/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*
	Small uillity functions.
*/

function Contains(a, c) {
	for (var i = 0; i < a.length; i++) {
		if (a[i] == c) return true;
	}
	return false;
}

function GetPos(i) {
	var r, c;
	var k = 1;

	for (var j = 0; j < board.n*board.n; j++)
	{
		if (j % board.n == 0 && j != 0) k++;
		if (i < k * board.n)
		{
			c = k - 1;
			r = i - ((k - 1) * board.n);
			break;
		}
	}

	return [r, c];
}

function GetInvPos(r, c) {
	if (r >= board.n || r < 0 || c >= board.n || c < 0) return -1;

	return r + c * board.n
}

/*
	Core game functions.
*/

function Restart() {
	var entries = new Array();
	var num = 0;
	var r = 0;
	var c = 0;
	var bomb = "B";

	for (var i = 0; i < /*board.n*board.n*/225; i++) {
		try
		{
			board.children[i].children[0].text = "";
		}
		catch (any)
		{
			break;
		}
	}

	for (var i = 0; i < board.mines; i++) {
		entries.push(Math.floor(Math.random() * board.n * board.n));
	}

	for (var i = 0; i < board.mines; i++) {
		board.children[entries[i]].children[0].text = bomb;
	}

	for (var i = 0; i < board.n*board.n; i++) {
		board.children[i].children[0].bomb = false;
		board.children[i].children[0].nb = false;
		board.children[i].children[0].color = "white";
		board.children[i].color = "gray";
		board.children[i].children[1].visible = true;
		board.children[i].visible = true;
		if (!Contains(entries, i)) {
			num = 0;
			r = GetPos(i)[0];
			c = GetPos(i)[1];
			if (GetInvPos(r + 1, c) != -1) {
				if (board.children[GetInvPos(r + 1, c)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r + 1, c + 1) != -1) {
				if (board.children[GetInvPos(r + 1, c + 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r, c + 1) != -1) {
				if (board.children[GetInvPos(r, c + 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r - 1, c) != -1) {
				if (board.children[GetInvPos(r - 1, c)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r - 1, c - 1) != -1) {
				if (board.children[GetInvPos(r - 1, c - 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r, c - 1) != -1) {
				if (board.children[GetInvPos(r, c - 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r + 1, c - 1) != -1) {
				if (board.children[GetInvPos(r + 1, c - 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r - 1, c + 1) != -1) {
				if (board.children[GetInvPos(r - 1, c + 1)].children[0].text == bomb) num++;
			}

			board.children[i].children[0].text = num;
		}
	}

	for (var i = 0; i < board.n*board.n; i++) {
		board.children[i].children[0].visible = false;
		//board.children[i].visible = true;
		//board.children[i].children[1].visible = true;
	}

	console.log(board.children.length, board.n*board.n, board.n);
}

function Click(i, f) {
	if (f)
	{
		if (board.children[i].children[0].visible == false)
		{
			board.children[i].children[0].visible = true;
			board.children[i].children[0].fb = true;
			board.children[i].children[0].oldtext = board.children[i].children[0].text;
			board.children[i].children[0].text = "F";
		}
		else if (board.children[i].children[0].text == "F")
		{
			board.children[i].children[0].visible = false;
			board.children[i].children[0].fb = false;
			board.children[i].children[0].nfb = true;
			board.children[i].children[0].text = board.children[i].children[0].oldtext;
			board.children[i].children[0].oldtext = "";
		}

		return;
	}

	if (board.children[i].children[0].text != "" && board.children[i].children[0].text != "F"  
		&& board.children[i].children[0].visible != true) {
		board.children[i].children[0].visible = true;
		if (board.children[i].children[0].text == "B") {
			//timer.running = true;
			//board.winning = true;
			var time = 123;
			for (var i = 0; i < board.n*board.n; i++) {
				if (board.children[i].children[0].text == "B") {
					board.children[i].children[0].time = time;
					board.children[i].children[0].bomb = true;
					board.children[i].children[0].visible = false;
					time += 100;
				}
				else if (board.children[i].children[0].text == "F" &&
					board.children[i].children[0].oldtext != "B")
				{
					board.children[i].children[0].text = "X";
					board.children[i].children[0].time = time;
					board.children[i].children[0].bomb = true;
					time += 100;
				}
			}
			msg.displayText("You Lost");
			swipeview.currentIndex = 0;
			board.pause = false;
		} else {
			if (board.children[i].children[0].text == "0") {
				board.children[i].children[0].text = "";
				var r = GetPos(i)[0];
				var c = GetPos(i)[1];
				if (GetInvPos(r + 1, c) != -1) {
					Click(GetInvPos(r + 1, c), false);
				}
				if (GetInvPos(r + 1, c + 1) != -1) {
					Click(GetInvPos(r + 1, c + 1), false);
				}
				if (GetInvPos(r, c + 1) != -1) {
					Click(GetInvPos(r, c + 1), false);
				}
				if (GetInvPos(r - 1, c) != -1) {
					Click(GetInvPos(r - 1, c), false);
				}
				if (GetInvPos(r - 1, c - 1) != -1) {
					Click(GetInvPos(r - 1, c - 1), false);
				}
				if (GetInvPos(r, c - 1) != -1) {
					Click(GetInvPos(r, c - 1), false);
				}
				if (GetInvPos(r + 1, c - 1) != -1) {
					Click(GetInvPos(r + 1, c - 1), false);
				}
				if (GetInvPos(r - 1, c + 1) != -1) {
					Click(GetInvPos(r - 1, c + 1), false);
				}
			}

			board.children[i].children[0].nb = true;

			var notmines = 0;
			for (var i = 0; i < board.n*board.n; i++) {
				if (board.children[i].children[0].text != "B" &&
					board.children[i].children[0].text != "F" && 
					board.children[i].children[0].visible == true) {
					notmines++;
				}
			}

			if (notmines == board.n*board.n - board.mines)
			{
				msg.displayText( "You Won");
				swipeview.currentIndex = 0;
				board.pause = false;
			}
		}
	}
}
