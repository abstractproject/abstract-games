/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4

Rectangle
{
	Material.accent: "lightgreen"
	Material.theme: "Light"
	id: statusdialog
	visible: true
	color: "lightgreen"
	objectName: "statusdialog"
	height: 50

	property string text: "dummy"
	property string bg: statusdialog.color

	SequentialAnimation
        {
                running: true
                loops: Animation.Infinite

                ColorAnimation
                {
                        properties: "color"
                        target: statusdialog
                        duration: 5000
                        from: "lightgreen"
                        to: "lightblue"
                }

                ColorAnimation
                {
                        properties: "color"
                        target: statusdialog
                        duration: 5000
                        from: "lightblue"
                        to: "orange"
                }

                ColorAnimation
                {
                        properties: "color"
                        target: statusdialog
                        duration: 5000
                        from: "orange"
                        to: "lightgreen"
                }
        }


	Rectangle
	{
		anchors.fill: parent
		color: statusdialog.bg

		Row
		{
			anchors.centerIn: parent

			Rectangle
			{
				color: statusdialog.bg
				width: statusdialog.width / 2.0
				height: statusdialog.height / 2.0

				Text
				{
					anchors.left: parent.left
					anchors.leftMargin: 10
					anchors.verticalCenter: parent.verticalCenter
					text: statusdialog.text
				}
			}
		

			Rectangle
			{
				color: statusdialog.bg
				width: statusdialog.width / 2.0
				height: statusdialog.height / 2.0

				Button
				{
					anchors.right: parent.right
					anchors.rightMargin: 10
					anchors.verticalCenter: parent.verticalCenter
					text: "Okay"

					onClicked: {
						Qt.quit();
					}
				}
			}
		}
	}
}
