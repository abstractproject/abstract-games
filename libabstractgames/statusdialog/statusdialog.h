/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <QDialog>
#include <QQuickStyle>
#include <QQmlEngine>
#include <QQuickItem>

//#include "src/ui_statusdialog.h"


QT_BEGIN_NAMESPACE

namespace Ui {
	class StatusDialog;
}

QT_END_NAMESPACE


class StatusDialog : public QDialog
{
	Q_OBJECT

public:
	StatusDialog(QWidget* parent = nullptr, QString text = "");
 
	~StatusDialog();

private:
	Ui::StatusDialog *ui;
};
