/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "statusdialog.h"
//#include <qDebug>

#include "src/ui_statusdialog.h"

StatusDialog::StatusDialog(QWidget* parent, QString text) : QDialog(parent), ui(new Ui::StatusDialog)
{
	QQuickStyle::setStyle("Material");

	ui->setupUi(this);

	setFixedSize(300, 50);

	ui->gridLayout->setContentsMargins(0, 0, 0, 0);

	connect(ui->quickWidget->engine(), &QQmlEngine::quit, this, &QDialog::close);

	//qDebug() << text << ui->quickWidget->rootObject()->findChild<QObject*>("statusdialog");
	ui->quickWidget->rootObject()/*->findChild<QObject*>("statusdialog")*/->setProperty("text", QVariant(text));

	exec();
}
 
StatusDialog::~StatusDialog()
{
	delete ui;
}
