/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4

Rectangle
{
	Material.accent: "lightgreen"
	Material.theme: "Light"

	id: tiles

	width: 360
	height: 360

	property bool pause: false
	property var _menu: menu

	signal newGame()
	signal restartGame()

	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
			menu.dismiss();
		}
	}

	Row
	{
		anchors.top: tiles.top
		anchors.topMargin: 10
		anchors.horizontalCenter: tiles.horizontalCenter
		spacing: 6
		id: buttons

		ToolButton
		{
			icon.source: "images/start.svg"
			width: 50
			height: 50

			onClicked:
			{
				if (tiles.pause)
				{
					if (!menu.opened)
					{
						menu.popup();
					}
					else
					{
						menu.dismiss();
					}
				}
				else
				{
					tiles.newGame();
				}
			}

			Menu
			{
				id: menu
				MenuItem
				{
					text: "Resume"

					onTriggered:
					{
						tiles.newGame();
					}
				}

				MenuItem
				{
					text: "Restart"

					onTriggered:
					{
						tiles.restartGame();
					}
				}
			}
		}
	}

	Timer
	{
		repeat: true
		interval: 4500
		running: true

		onTriggered:
		{
			tiprect.newTip();
		}
	}

	Rectangle
	{
		anchors.bottom: tiles.bottom
		anchors.bottomMargin: 10
		anchors.left: tiles.left
		anchors.right: tiles.right
		color: tiles.color
		id: tiprect

		property int i: 0
		property var tips:
			["<center>Swipe to Begin, Pause, and Continue Games</center>",
			"<center>If you want Help, Look at the <a href='abstractproject.bitbucket.io'>abstractproject.bitbucket.io</a></center>",
			"<center>Look in your Launcher, and try Opening SameGame</center>",
			"<center>Pause and Click on Start to see Options to Restart</center>",
			"<center>In Mines, Right Click to Flag an Area</center>"]

		property var newTip: function()
		{
			if (++i > tiprect.tips.length) i = 0;
			tip.text = tiprect.tips[i];
			op.start();
		}


		NumberAnimation on opacity
		{
			id: op
			duration: 1200
			from: 0
			to: 1
		}

		Label
		{
			Material.accent: "black"

			id: tip
			anchors.centerIn: parent
			text: "<center>Swipe to Begin, Pause, and Continue Games</center>"
		}
	}
}
