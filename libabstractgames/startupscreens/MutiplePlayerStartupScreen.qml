/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4

Rectangle {
        signal newGame()
	signal restartGame()
	signal displayText(string text)
    
        Material.accent: "lightgreen"
        Material.theme: "Light"

	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
			tiles._menu.dismiss();
		}
	}

    

		//anchors.centerIn: parent
		//anchors.fill: parent
		id: msg

		property bool spectrumonone: false 
		property bool spectrumontwo: true
       		property string text: "Click Here to Begin a Game"
		property bool pause: false

		SequentialAnimation
		{
			running: true
			loops: Animation.Infinite

			ColorAnimation
			{
				properties: "color"
				target: msg
				duration: 5000
				from: "lightgreen"
				to: "lightblue"
			}

			ColorAnimation
			{
				properties: "color"
				target: msg
				duration: 5000
				from: "lightblue"
				to: "orange"
			}

			ColorAnimation
			{
				properties: "color"
				target: msg
				duration: 5000
				from: "orange"
				to: "lightgreen"
			}
		}

		Column {
			anchors.centerIn: parent
			anchors.fill: parent

			Tiles
			{
				width: window.width 
				height: window.height / 2
				color: msg.color
				pause: msg.pause
				id: tiles

				onNewGame:
				{
					msg.newGame();
				}

				onRestartGame:
				{
					msg.restartGame();
				}
			}

			Rectangle
			{
				width: window.width
				height: window.height / 2
				color: msg.color

				SpinBox
				{
					anchors.bottom: parent.bottom
					anchors.bottomMargin: 10
					anchors.horizontalCenter: parent.horizontalCenter
					value: 1
					from: msg.pause ? value : 1
					to: msg.pause ? value : 2

					textFromValue: function(value)
					{
						if (value == 1) return "1 Player"
						else return "2 Players"
					}

					valueFromText: function(text)
					{
						if (text == "1 Player") return 1
						else return 2;
					}


					onValueModified: {
						if (displayText == "1 Player")
						{
							msg.spectrumontwo = true;
						}
						else
						{
							msg.spectrumontwo = false;
						}
					}
				}
			}
		}
	}
