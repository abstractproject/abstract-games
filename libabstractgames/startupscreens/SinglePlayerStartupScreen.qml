/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4 

Rectangle {
	id: msg
	signal newGame()
	signal restartGame()
	signal displayText(string text)


	color: "lightgreen"
	property string text: "Start Here"
	property bool easy: false
	property bool medium: true
	property bool pause: false

	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
			tiles._menu.dismiss();
		}
	}


	SequentialAnimation
	{
		running: true
		loops: Animation.Infinite

		ColorAnimation
		{
			properties: "color"
			target: msg
			duration: 5000
			from: "lightgreen"
			to: "lightblue"
		}

		ColorAnimation
		{
			properties: "color"
			target: msg
			duration: 5000
			from: "lightblue"
			to: "orange"
		}

		ColorAnimation
		{
			properties: "color"
			target: msg
			duration: 5000
			from: "orange"
			to: "lightgreen"
		}
	}

	Column
	{
		Tiles
		{
			width: window.width 
			height: window.height / 2
			color: msg.color
			pause: msg.pause
			id: tiles

			onNewGame:
			{
				msg.newGame();
			}

			onRestartGame:
			{
				msg.restartGame();
			}
		}

		Rectangle
		{
			width: window.width
			height: window.height / 2
			color: msg.color

			SpinBox
			{
				anchors.bottom: parent.bottom
				anchors.horizontalCenter: parent.horizontalCenter
				anchors.bottomMargin: 10

				value: 2
				from: msg.pause ? value : 1
				to: msg.pause ? value :  3

				textFromValue: function(value)
				{
					if (value == 1) return "Easy"
					else if (value == 2) return "Medium"
					else return "Hard"
				}

				valueFromText: function(text)
				{
					if (text == "Easy") return 1
					else if (txt == "Medium") return 2
					else return 3;
				}


				onValueModified: {
					if (displayText == "Easy")
					{
						msg.easy = true;
					}
					else if (displayText == "Medium")
					{
						msg.medium = true;
					}
					else
					{
						msg.easy = false;
						msg.medium = false;
					}
		   		 }
			}
		}
	}
}
