/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "gamewindow.h"
#include <QDebug>

#include "src/ui_gamewindow.h"

GameWindow::GameWindow(QString icon, QString name, QString url, bool saving) : QMainWindow(), ui(new Ui::GameWindow)
{
	QQuickStyle::setStyle("Material");

	ui->setupUi(this);

	Icon = icon;
	Name = name;
	Url = url;
	Saving = saving;

	setWindowIcon(QIcon::fromTheme(Icon));
	setWindowTitle(Name);
	//setMinimumSize(360, 360);

	setFixedSize(500, 500);

	ui->gridLayout->setContentsMargins(0, 0, 0, 0);
	ui->actionAbstract_Games_Help->setText(Name + " Help");

	//connect(ui->quickWidget->rootObject()->findChild<QObject*>("msg"), SIGNAL(closeGame()), SLOT(CloseGame()));
	connect(ui->quickWidget->rootObject()->findChild<QObject*>("msg"), SIGNAL(displayText(QString)), SLOT(DisplayText(QString)));

	show();
}
 
GameWindow::~GameWindow()
{
	delete ui;
}

void GameWindow::CloseGame()
{
	close();
}

void GameWindow::SaveGame()
{
	if (Saving)
	{
		QJSValue moves = ui->quickWidget->rootObject()->children()[0]->property("save").value<QJSValue>().call();
		int length = moves.property(0).property("length").toInt();

		qDebug() << length;

		QFileDialog dialog;
		dialog.setAcceptMode(QFileDialog::AcceptSave);
		dialog.setNameFilter("Saved Game (*.savegame)");

		if (dialog.exec() != QDialog::Rejected)
		{
			QFile file(dialog.selectedFiles()[0]);
			if (file.open(QFile::WriteOnly | QFile::Truncate))
			{
				QTextStream stream(&file);

				for (int i = 0; i < length; i++)
				{
					stream << moves.property(0).property(i).toString() << ' ';
					stream << moves.property(1).property(i).toString() << ' ';
				}
			}
		}
	}
}

void GameWindow::OpenGame()
{
	if (Saving)
	{
		QFileDialog dialog;
		dialog.setAcceptMode(QFileDialog::AcceptOpen);
		dialog.setNameFilter("Saved Game (*.savegame)");

		if (dialog.exec() != QDialog::Rejected)
		{
			QFile file(dialog.selectedFiles()[0]);
			if (file.open(QFile::ReadOnly))
			{
				QTextStream stream(&file);
				QVector<QString> vector = QVector<QString>();
				QString string;

				while (!stream.atEnd())
				{
					stream >> string;
					if (string != "")
					{
						vector.append(string);
					}
				}

				QJSValue moves = ui->quickWidget->rootObject()->children()[0]->property("moves").value<QJSValue>();
				QJSValue colors = ui->quickWidget->rootObject()->children()[0]->property("colors").value<QJSValue>();
				moves.setProperty("length", vector.length() / 2);
				colors.setProperty("length", vector.length() / 2);

				qDebug() << vector;

				for (int i = 0, j = 0; i < vector.length(); i += 2, j++)
				{
					moves.setProperty(j, vector[i]);
					colors.setProperty(j, vector[i + 1]);
				}

				ui->quickWidget->rootObject()->children()[0]->property("open").value<QJSValue>().call();
			}
		}
	}
}

void GameWindow::DisplayText(QString text)
{
	StatusDialog* sd = new StatusDialog(this, text);
}
