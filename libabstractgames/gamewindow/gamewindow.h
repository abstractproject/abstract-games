/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <QMainWindow>
#include <QIcon>
#include <QFile>
#include <QFileDialog>
#include <QQmlEngine>
#include <QDebug>
#include <QQuickItem>
#include <QMessageBox>
#include <QDesktopServices>
#include <QPixmap>
#include <QQuickStyle>

//#include "src/ui_gamewindow.h"
#include "../statusdialog/statusdialog.h"

QT_BEGIN_NAMESPACE

namespace Ui {
	class GameWindow;
}

QT_END_NAMESPACE


class GameWindow : public QMainWindow
{
	Q_OBJECT

public:
	GameWindow(QString icon = "", QString name = "", QString url = "", bool saving = false);
 
	~GameWindow();

public Q_SLOTS:
	void CloseGame();

	void SaveGame();

	void OpenGame();

	void DisplayText(QString text);

private:
	Ui::GameWindow *ui;

	QString Icon;
	QString Name;
	QString Url;
	
	bool Saving;
};
