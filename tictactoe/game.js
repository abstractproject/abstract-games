/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*
	Small utillity functions.
*/

function CanMakeThree()
{
	var moves = new Array();

	for (var i = 0; i < board.n*board.n; i++)
	{
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + 1] != undefined && board.children[i + 1].children[1] != undefined)
			{
				console.log("Bye (1)");
				if (board.children[i].children[1].text == board.children[i + 1].children[1].text)
				{
					console.log("Hey (1)");
					if (board.children[i + 2] != undefined && board.children[i + 2].children[1] != undefined)
					{
						console.log("Hey (2)", i, board.children[i + 2].children[1].text);
						if (board.children[i + 2].children[1].text == "")
						{
							if (((i + 1) % board.n != 0) && ((i + 2) % board.n != 0)) 
								moves.push([i + 2, 
									board.children[i].children[1].text == board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + 2] != undefined && board.children[i + 2].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + 2].children[1].text)
				{
					if (board.children[i + 1] != undefined && board.children[i + 1].children[1] != undefined)
					{
						if (board.children[i + 1].children[1].text == "")
						{
							if (((i + 1) % board.n != 0) && ((i + 2) % board.n != 0)) 
								moves.push([i + 1, 
									board.children[i].children[1].text == board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + 1] != undefined && board.children[i + 1].children[1] != undefined)
			{
				if (board.children[i + 2] != undefined && board.children[i + 2].children[1] != undefined)
				{
					if (board.children[i + 1].children[1].text == board.children[i + 2].children[1].text)
					{
						if (board.children[i].children[1].text == "")
						{
							if (((i + 1) % board.n != 0) && ((i + 2) % board.n != 0)) 
								moves.push([i, 
									board.children[i + 1].children[1].text == board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + board.n] != undefined && board.children[i + board.n].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + board.n].children[1].text)
				{
					if (board.children[i + board.n*2] != undefined && 
						board.children[i + board.n*2].children[1] != undefined)
					{
						if (board.children[i + board.n*2].children[1].text == "")
						{
							moves.push([i + board.n*2, 
								board.children[i].children[1].text == board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + board.n*2] != undefined && board.children[i + board.n*2].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + board.n*2].children[1].text)
				{
					if (board.children[i + board.n] != undefined && 
						board.children[i + board.n].children[1] != undefined)
					{
						if (board.children[i + board.n].children[1].text == "")
						{
							moves.push([i + board.n, 
								board.children[i].children[1].text == board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + board.n*2] != undefined && board.children[i + board.n*2].children[1] != undefined)
			{
				if (board.children[i + board.n] != undefined && board.children[i + board.n].children[1] != undefined)
				{
					if (board.children[i + board.n*2].children[1].text == board.children[i + board.n].children[1].text)
					{
						if (board.children[i].children[1].text == "")
						{
							moves.push([i, board.children[i + board.n*2].children[1].text == 
								board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + (board.n + 1)] != undefined && board.children[i + (board.n + 1)].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + (board.n + 1)].children[1].text)
				{
					if (board.children[i + (board.n + 1)*2] != undefined && 
						board.children[i + (board.n + 1)*2].children[1] != undefined)
					{
						if (board.children[i + (board.n + 1)*2].children[1].text == "")
						{
							if ((i + (board.n + 1)*2) % board.n != 0)
								moves.push([i + (board.n + 1)*2, 
									board.children[i].children[1].text == board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + (board.n + 1)*2] != undefined 
				&& board.children[i + (board.n + 1)*2].children[1] != undefined)
			{
				if (board.children[i + (board.n + 1)*2].children[1].text == 
					board.children[i + (board.n + 1)].children[1].text)
				{
					if (board.children[i + (board.n + 1)] != undefined 
						&& board.children[i + (board.n + 1)].children[1] != undefined)
					{
						if (board.children[i + (board.n + 1)].children[1].text == "")
						{
							if ((i + (board.n + 1)*2) % board.n != 0)
								moves.push([i + (board.n + 1), board.children[i].children[1].text == 
									board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + (board.n + 1)*2] != undefined && 
				board.children[i + (board.n + 1)*2].children[1] != undefined)
			{
				if (board.children[i + (board.n + 1)] != undefined && 
					board.children[i + (board.n + 1)].children[1] != undefined)
				{
					if (board.children[i + (board.n + 1)*2].children[1].text == 
						board.children[i + (board.n + 1)].children[1].text)
					{
						if (board.children[i].children[1].text == "")
						{
							if ((i + (board.n + 1)*2) % board.n != 0)
								moves.push([i, board.children[i + (board.n + 1)*2].children[1].text == 
								board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + (board.n - 1)] != undefined && board.children[i + (board.n - 1)].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + (board.n - 1)].children[1].text)
				{
					if (board.children[i + (board.n - 1)*2] != undefined && 
						board.children[i + (board.n - 1)*2].children[1] != undefined)
					{
						console.log(board.children[i + (board.n - 1)*2].children[1].text);
						if (board.children[i + (board.n - 1)*2].children[1].text == "")
						{
							if (((i + (board.n - 1)) % board.n != 0) &&
								((i + board.n) % board.n != 0))
								moves.push([i + (board.n - 1)*2, board.children[i].children[1].text == 
								board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + (board.n - 1)*2] != undefined && 
				board.children[i + (board.n - 1)*2].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + (board.n - 1)*2].children[1].text)
				{
					if (board.children[i + (board.n - 1)] != undefined && 
						board.children[i + (board.n - 1)].children[1] != undefined)
					{
						if (board.children[i + (board.n - 1)].children[1].text == "")
						{
							if (((i + (board.n - 1)) % board.n != 0) &&
								((i + board.n) % board.n != 0))
								moves.push([i + (board.n - 1), 
									board.children[i].children[1].text == board.player ? 1 : 0]);
						}
					}
				}
			}

			if (board.children[i + (board.n - 1)*2] != undefined && 
				board.children[i + (board.n -  1)*2].children[1] != undefined)
			{
				if (board.children[i + (board.n - 1)] != undefined && 
					board.children[i + (board.n - 1)].children[1] != undefined)
				{
					if (board.children[i + (board.n - 1)*2].children[1].text == 
						board.children[i + (board.n - 1)].children[1].text)
					{
						if (board.children[i].children[1].text == "")
						{
							if (((i + (board.n - 1)) % board.n != 0) &&
								((i + board.n) % board.n != 0))
								moves.push([i,board.children[i + (board.n - 1)*2].children[1].text == 
									board.player ? 1 : 0]);
						}
					}
				}
			}
		}
	}

	console.log(moves);

	return moves;
}

/*
	Game file save/load functions.
*/

function Save()
{
	var moves = new Array();
	var colors = new Array();

	for (var i = 0; i < board.n*board.n; i++)
	{
		if (board.children[i].children[1].text != "")
		{
			moves.push(i);
			colors.push(board.children[i].children[1].text);
		}
	}

	return [moves, colors];
}

function Open()
{
	Restart();
	for (var i = 0; i < obj1.moves.length; i++)
	{
		board.children[obj1.moves[i]].children[1].text = obj1.colors[i];
		if (obj1.colors[i] == "X")
			board.children[obj1.moves[i]].clickedred = true;
		else
			board.children[obj1.moves[i]].clickedblue = true;
	}
}

function PlayAI() {
	var moves = CanMakeThree();

	if (moves.length == 0)
	{
		for (var index = 0; index < board.n*board.n; index++) {
			if (board.children[index].children[1].text == "") {
				moves.push([index, 1]);
			}
		}
	}

	var GoodMoves = new Array();

	for (var i = 0; i < moves.length; i++)
	{
		if (moves[i][1] == 1) GoodMoves.push(moves[i][0]);
	}

	if (GoodMoves.length == 0)
	{
		console.log("UI");
		for (var i = 0; i < moves.length; i++)
		{
			if (moves[i][1] == 0) GoodMoves.push(moves[i][0]);
		}
		
		if (GoodMoves.length == 0)
		{
			console.log("YU");
			for (var i = 0; i < moves.length; i++)
			{
				if (moves[i][1] == 2) GoodMoves.push(moves[i][0]);
			}

			if (GoodMoves.length == 0)
			{
				console.log("BU");
				for (var i = 0; i < moves.length; i++)
				{
					GoodMoves.push(moves[i][0]);
				}
			}
		}
	}

	var move = GoodMoves[Math.floor(Math.random() * GoodMoves.length)];

	board.children[move].children[1].text = board.player;
	if (board.player == "X")
		board.children[move].clickedred = true;
	else
		board.children[move].clickedblue = true;
}

function AlertUser() {
	if (CheckForWin() == "X") {
		if (msg.playaix)
		{
			msg.displayText("Spectrum Won");
		}
		else
		{
			msg.displayText("X Won");
		}
		swipeview.currentIndex = 0;
		board.pause = false;
		return;
	} else if (CheckForWin() == "O") {
		if (msg.playaio)
		{
			msg.displayText("Spectrum Won");
		}
		else
		{
			msg.displayText("O Won");
		}
		swipeview.currentIndex = 0;
		board.pause = false;
		return;
	}

	if (CheckForDraw()) {
		msg.displayText("Draw");
		swipeview.currentIndex = 0;
		board.pause = false;
	}
}

function CheckForDraw() {
	for (var index = 0; index < board.n*board.n; index++) {
		if (board.children[index].children[1].text == "")
			return false;
	}
	return true;
}

function CheckForWin() {
	for (var i = 0; i < board.n*board.n; i++)
	{
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + 1] != undefined && board.children[i + 1].children[1] != undefined) 
			{
				if (board.children[i].children[1].text == board.children[i + 1].children[1].text)
				{
					if (board.children[i + 2] != undefined && board.children[i + 2].children[1] != undefined)
					{
						if (board.children[i].children[1].text == board.children[i + 2].children[1].text)
						{
							if (((i + 1) % board.n != 0) && ((i + 2) % board.n != 0))
								return board.children[i].children[1].text;
						}
					}
				}
			}
		}
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + board.n] != undefined && board.children[i + board.n].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + board.n].children[1].text)
				{
					if (board.children[i + board.n*2] != undefined 
						&& board.children[i + board.n*2].children[1] != undefined)
					{
						if (board.children[i + board.n*2].children[1].text == board.children[i].children[1].text)
						{
							return board.children[i].children[1].text;
						}
					}
				}
			}
		}
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + (board.n + 1)] != undefined  && board.children[i + (board.n + 1)].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + (board.n + 1)].children[1].text)
				{
					if (board.children[i + (board.n + 1)*2] != undefined 
						&& board.children[i + (board.n + 1)*2].children[1] != undefined)
					{
						if (board.children[i + (board.n + 1)*2].children[1].text 
							== board.children[i].children[1].text)
						{
							if ((i + (board.n + 1)*2) % board.n != 0)
								return board.children[i].children[1].text;
						}
					}
				}
			}
		}
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + (board.n - 1)] != undefined && board.children[i + (board.n - 1)].children[1] != undefined)
			{
				if (board.children[i].children[1].text == 
					board.children[i + (board.n - 1)].children[1].text)
				{
					if (board.children[i + (board.n - 1)*2] != undefined && 
						board.children[i + (board.n - 1)*2].children[1] != undefined)
					{
						if (board.children[i + (board.n - 1)*2].children[1].text == 
							board.children[i].children[1].text)
						{
							if (((i + (board.n - 1)) % board.n != 0) && 
								((i + board.n) % board.n != 0))
				 				return board.children[i].children[1].text;
						}
					}
				}
			}
		}
	}

	return "";
}

function Restart() {
	board.player = "X";

	for (var index = 0; index < board.n*board.n; index++) {
		board.children[index].children[1].text = "";
		board.children[index].clickedred = false;
		board.children[index].clickedblue = false;
		board.children[index].color = "gray";
	}

	if (msg.playaix) {
		PlayAI();
		board.player = "O";
	}
}

function Click(index) {
	if (board.children[index].children[1].text == "") {
	        if (board.player == "X") {
			board.children[index].children[1].text = "X";
			board.children[index].clickedred = true;

			board.player = "O";
		} else {
			board.children[index].children[1].text = "O";
			board.children[index].clickedblue = true;

			board.player = "X";
		}
	} else {
		return;
	}

	if ((msg.playaio || msg.playaix) && (!board.winning)) {
		PlayAI();

   		if (msg.playaio) {
        		board.player = "X";
   	 	} else {
        		board.player = "O";
    		}
	}

	AlertUser();
}

