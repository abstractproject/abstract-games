/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.11 
import QtQuick.Controls 2.4
import "game.js" as Game

Rectangle {
	id: window
	width: 360
	height: 360
	color: "gray"

	SwipeView
	{
		id: swipeview
		currentIndex: 0
		anchors.fill: parent

		MutiplePlayerStartupScreen {
			id: msg
			pause: board.pause
			objectName: "msg"
	
			property bool playaix: msg.spectrumonone
			property bool playaio: msg.spectrumontwo
	
			onNewGame:
			{
			    if (board.pause)
				{
					swipeview.currentIndex = 1;
					return;
				}

				swipeview.currentIndex = 1;
				Game.Restart();
			}
			
            onRestartGame: {
                swipeview.currentIndex = 1;
                Game.Restart();
            }
		}

		Grid {
			property int n: 3 

			id: board
			columns: n 
			rows: n 

			property string player: "X"
			property bool pause: false

			Repeater {
				model: board.n*board.n 

				Rectangle {
					width: window.width/board.n
					height: window.height/board.n
					color: "gray"
					id: square

					MouseArea {
						id: mousearea

						onClicked: {
							Game.Click(index);
						}

						anchors.centerIn: parent
						anchors.fill: parent
					}

					border {
						width: 1
						color: "black"
					}

					Text {
						id: text
						color: "#00000000"
					}

					Image {
						anchors.centerIn: parent
						smooth: true
						source: text.text == "X" ? "images/x.png" : "images/o.png"
						visible: text.text != "" 
					}

					property bool clickedred: false
					property bool clickedblue: false

					ColorAnimation {
						target: square 
						properties: "color"
						running: clickedred
						duration: 2500
						from: "gray"
						to: "red"
					}

					ColorAnimation {
						target: square 
						properties: "color"
						running: clickedblue
						duration: 2500
						from: "gray"
						to: "blue"
					}
				}
			}
		}
	}

	QtObject
	{
		property int index: swipeview.currentIndex

		Behavior on index
		{
			ScriptAction
			{
				script:
				{
					if (swipeview.currentIndex == 1)
					{
						if (!board.pause)
						{
							Game.Restart();
						}
					}
					else
					{
						board.pause = true;
					}
				}
			}
		}
	}
}
