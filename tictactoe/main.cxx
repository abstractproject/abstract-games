/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <QApplication>
#include <QQuickView>
#include <QQmlEngine>
#include <QUrl>
#include <QIcon>

#include "../libabstractgames/gamewindow/gamewindow.h"

int main(int argc, char* argv[])
{
	QApplication* app = new QApplication(argc, argv);
	GameWindow* win = new GameWindow("bovo", "TicTacToe", 
			"https://bitbucket.org/abstractproject/abstract-games/wiki/tictactoe/Home.md", true);

	return app->exec();
}
