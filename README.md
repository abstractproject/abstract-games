# We've moved to GitLab

Hello, all Abstract Games users. Because of Bitbucket's decision to remove all Mecurial repositories in 2020, like many other people we have decided to move to GitLab. You can now submit bug reports and view the source code at:
https://gitlab.com/abstractsoftware/abstract-games
Thank you for your support and we hope you enjoy Abstract Games,
The Abstract Software Team.